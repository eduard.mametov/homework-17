#include <iostream>
#include <cmath>
using namespace std;

class Vector
{
private:
	double x;
	double y;
	double z;

public:
	Vector(int _x, int _y, int _z) : x(_x), y(_y), z(_z)
	{
	}

	Vector()
	{
		x = 0;
		y = 0;
		z = 0;
	}

	void Show()
	{
		cout << "X: " << x << '\n' << "Y: " << y << "\n" << "Z: " << z << '\n';
	}

	double lenght()
	{
		int LenghtVector = (x*x) + (y*y) + (z*z);
		cout << '\n' << "vector length: " 
			<< sqrt(LenghtVector) << '\n';
		
		return LenghtVector;
	}
};

int main()
{
	Vector temp(1, 2, 3);
	temp.Show();
	temp.lenght();

	return 0;
}